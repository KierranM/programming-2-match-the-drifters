﻿/*-----------------------------------------------------------------------------------------
 *
 *                                   Title / General Descriptor
 *
 *------------------------------------------------------------------------------------------
 *
 *
 *
 * Program name: Match The Drifters
 *
 * Project file name: Match The Drifters.sln
 *
 * Author:          Kierran McPherson
 *
 * Date:            27/08/2012
 * 
 * Language:        C#
 * 
 * Platform:        Microsoft Visual Studio 2010 Ultimate
 * 
 * Purpose:         Match the Drifters is a memory game where the user selects tiles in order to match images. The faster the user matches tiles the higher the score they get.
 * 
 * Description:     This program consists of 3 forms. The first is the menu form which allows the user to select a difficulty, open up form3 which allows the user to add there own images to the image pool. 
 *                  And then open form2 which is the game itself. Once in form2 all the images are loaded and then placed randomly into invisible pictureBoxes. The timer starts and the user has until the timer
 *                  reaches 0 to match all the squares. Gaining 5 seconds for every successful match. Once time is out or the user selects all the pairs. the user will be asked if they wish to start again.
 *                  All high scores are saved along with the number of pairs of images in the MTD.dat file
 * 
 * Known Bugs:      None known as yet
 * 
 * Additional Features:
 *                  User can browse for image files to match
 *                  
 *
 *
 *
 *
 * 
 * ------------------------------------------------------------------------------------------
 * 
 *                                           Code space
 * 
 * ------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Match_The_Drifters
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure You Wish To Quit?", "You Want To Quit?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Environment.Exit(0);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            if (radioButton1.Checked)
            {
                form2.Difficulty = "easy";
            }
            else
            {
                form2.Difficulty = "hard";
            }
            Hide();
            form2.Show(this);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog(this);

        }
    }
}
