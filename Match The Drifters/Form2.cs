﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace Match_The_Drifters
{
    public partial class Form2 : Form
    {
        public string Difficulty { get; set; } //will only be hard or easy

        Random random = new Random();

        Image[,] allImages;
        Image[] easyImages = new Image[8];
        Image[] hardImages = new Image[8];
        Image panelBackground = Image.FromFile(@"Resources\Images\back.bmp");

        PictureBox[] pBoxes = new PictureBox[16];

        Panel[] panels = new Panel[16];

        int firstPanelNo;
        int secondPanelNo;
        int highScore;
        int numberOfImagePairs;
        int mouseClicks = 0;
        int score = 0;
        
        const int EASYTIME = 30;
        const int HARDTIME = 60;
        const int TIMEINCREASE = 5;
        const string EASY = "easy";

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            GetDataFromTXTFile();
            SetAllImageArraySize();
            SetUpPanelArray();
            SetUpPBoxArray();
            LoadAllImages();
            SetUpNewGame();
        }

        private void GetDataFromTXTFile()
        {
            StreamReader read = new StreamReader(@"Resources\MTD.txt");

            highScore = Convert.ToInt32(read.ReadLine());
            numberOfImagePairs = Convert.ToInt32(read.ReadLine());

            read.Close();
        }

        private void SetAllImageArraySize()
        {
            allImages = new Image[numberOfImagePairs, 2];
        }

        private Control FindPanelControl(string name) //Used for filling the panel array
        {
            foreach (Control control in Controls)
            {
                if (control.Name == name)
                {
                    return control;
                }
            }
            return null;
        }

        private Control FindPBoxControl(string name) //Used for finding the pictureBoxes inside the panels
        {
            foreach (Panel panel in panels)
            {
                foreach (Control control in panel.Controls)
                {
                    if (control.Name == name)
                    {
                        return control;
                    }
                }
            }
            return null;
        }

        private void SetUpPanelArray() //Fills the panels array
        {
            for (int i = 0; i < panels.Length; i++)
            {
                panels[i] = (Panel)FindPanelControl("panel" + (i + 1).ToString());
            }
        }

        private void SetUpPBoxArray() // fills the pBoxes array
        {
            for (int i = 0; i < pBoxes.Length; i++)
            {
                pBoxes[i] = (PictureBox)FindPBoxControl("pictureBox" + (i + 1).ToString());
            }
        }

        private void LoadAllImages() //loads all the images. The main image is placed in the first column. And its partner image (for hard) is placed in the second column.
        {
            for (int i = 0; i < allImages.Length / 2; i++)
            {
                allImages[i, 0] = Image.FromFile(@"Resources\Images\" + (i + 1).ToString() + "-0.bmp");
                allImages[i, 1] = Image.FromFile(@"Resources\Images\" + (i + 1).ToString() + "-1.bmp");
            }
        }

        private void SetUpNewGame() //Runs all the methods required to make a new game
        {
            score = 0; //reset score
            GetDataFromTXTFile();
            label2.Text = score.ToString();
            label6.Text = highScore.ToString();
            ResetTimer();
            ClearGameImages();
            ClearPBoxes();
            MakePanelsVisible();
            LoadGameImages();
            SetPanelBackgrounds();
            ChangePanelEnableState(true);
            SetPictureBoxesInvisible();
            PlaceImagesInPBoxes();
        }

        private void ResetTimer() //Resets label4.Text to the appropriate time. And enables the timer
        {
            if (Difficulty == EASY)
            {
                label4.Text = EASYTIME.ToString();
            }
            else
            {
                label4.Text = HARDTIME.ToString();
            }
            timer1.Enabled = true;
        }

        private void ClearGameImages() //Removes all images from the easyImages and hardImages array
        {
            for (int i = 0; i < easyImages.Length; i++)
            {
                easyImages[i] = null;
                hardImages[i] = null;
            }
        }

        private void ClearPBoxes() //Removes any pictures from the picturebox arrays
        {
            for (int i = 0; i < pBoxes.Length; i++)
            {
                pBoxes[i].Image = null;
            }
        }

        private void MakePanelsVisible() //Makes all Panels in the panels array visible
        {
            foreach (Panel panel in panels)
            {
                panel.Visible = true;
            }
        }

        private void LoadGameImages() //fills the easy and hard array with a series of random images. The hard array gets the hard match image of the easy image
        {
            int rand;

            for (int i = 0; i < easyImages.Length; i++)
            {
                rand = random.Next(allImages.Length / 2);
                while (easyImages.Contains(allImages[rand, 0]))
                {
                    rand = random.Next(allImages.Length / 2);
                }
                easyImages[i] = allImages[rand, 0];
                hardImages[i] = allImages[rand, 1];
            }
        }

        private void SetPanelBackgrounds() //Sets all panel backgrounds to the panelBackground image
        {
            foreach (Panel panel in panels)
            {
                panel.BackgroundImage = panelBackground;
            }
        }

        private void ChangePanelEnableState(bool state) //Enables or disables all panels
        {
            foreach (Panel panel in panels)
            {
                panel.Enabled = state;
            }
        }

        private void SetPictureBoxesInvisible() //Makes all elements in pBoxes invisible
        {
            foreach (PictureBox pBox in pBoxes)
            {
                pBox.Visible = false;
            }
        }

        private void PlaceImagesInPBoxes() //places images in the invisible pictureBoxes
        {
            if (Difficulty == EASY)
            {

                ChoosePositions(easyImages);

            }
            else
            {
                ChoosePositions(hardImages);
            }
        }

        private void ChoosePositions(Image[] imageArray) //Places the images in easyImages into random pictureboxes
        {
            int place1;
            int place2;
            for (int i = 0; i < easyImages.Length; i++)
            {
                place1 = random.Next(pBoxes.Length);
                while (pBoxes[place1].Image != null) //Check if there is already an image in the pBox
                {
                    place1 = random.Next(pBoxes.Length);
                }
                pBoxes[place1].Image = easyImages[i];

                place2 = random.Next(pBoxes.Length);
                while (pBoxes[place2].Image != null)
                {
                    place2 = random.Next(pBoxes.Length);
                }
                pBoxes[place2].Image = imageArray[i]; //Place the second image in the pBox. The second image is taken from the Image array that is passed in.
            }
        }

        private void button2_Click(object sender, EventArgs e) //Return to Main Menu Button
        {
            timer1.Enabled = false;
            if (MessageBox.Show("Are you sure you wish to return to the main menu?\r\nYou will lose your current progress", "Are you sure?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Owner.Show(); //Shows form1
                this.Close();
            }
            else
            {

                timer1.Enabled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e) //Quit Button
        {
            Quit();
        }

        private void Quit() //Quits the game if the user clicks yes in the MessageBox
        {
            timer1.Enabled = false;
  
            if (MessageBox.Show("Are you sure you wish to quit?\r\nYou will lose your current progress", "Are you sure?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Environment.Exit(0);
            }

            timer1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e) //Start New Game Button
        {
            timer1.Enabled = false;

            if (MessageBox.Show("Are you sure you wish to start again?\r\nYou will lose your current progress", "Are you sure?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                SetUpNewGame();
            }

            timer1.Enabled = true;
        }

        private void panel_Click(object sender, EventArgs e) //Generic panel click handler
        {
            Panel panel = sender as Panel;
            int panelNo = Convert.ToInt32(panel.Name.Substring(5)) - 1;

            mouseClicks++;
            if (mouseClicks == 1) //First panel
            {
                firstPanelNo = panelNo; //store first panels number
                pBoxes[panelNo].Visible = true;
                panels[panelNo].BackgroundImage = null;
                panels[panelNo].Enabled = false;
            }
            else //second panel
            {
                panels[panelNo].BackgroundImage = null;
                
                pBoxes[panelNo].Visible = true;
                secondPanelNo = panelNo;
                panels[panelNo].Enabled = false;
                ChangePanelEnableState(false);
                CheckForMatch();
                EnableVisiblePanels();
                mouseClicks = 0; //reset the number of mouseClicks

               
            }

            CheckForGameFinished();
        }

        private void CheckForGameFinished() //Check if the game is finished
        {
            int noOfMatches = 0;

            foreach (Panel p in panels)
            {
                if (p.Visible == false)
                {
                    noOfMatches += 1;
                }
            }

            noOfMatches /= 2;

            if (noOfMatches == 8)
            {
                timer1.Enabled = false;
                bool win;
                win = CheckHighScore();
                if (win == true)
                {
                    
                    if (MessageBox.Show("You beat your previous score of " + highScore.ToString() + " by " + (score - highScore).ToString() + "\r\nYour new high score is " + score.ToString() + "\r\n\r\n\tWould you like to play again?", "You Won!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        SetUpNewGame();
                    }
                    else
                    {
                        Environment.Exit(0);
                    }
                }
                else
                {
                    if (MessageBox.Show("You matched all the tiles but did not beat your high score of " + highScore.ToString() + "\r\n\n\t\tWould you like to play again?", "You Won!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        SetUpNewGame();
                    }
                    else
                    {
                        Environment.Exit(0);
                    }
                }
                
            }
        }

        private bool CheckHighScore() //Checks if current score is greater than recorded high score
        {
            if (score > highScore)
            {
                
                StreamWriter write = new StreamWriter(@"Resources\MTD.txt");

                write.WriteLine(score.ToString());
                write.WriteLine(numberOfImagePairs.ToString());

                write.Close();

                return true;
            }
            else
            {
                return false;
            }
        }

        private void CheckForMatch() //checks if the two images match
        {
            bool correct = false;
            if (Difficulty == EASY)
            {
                if (pBoxes[firstPanelNo].Image == pBoxes[secondPanelNo].Image) //Direct image comparison
                {
                    correct = true;
                }
            }
            else
            {
                for (int i = 0; i < easyImages.Length; i++)
                {
                    //The first panel pictureBox must match the image in easyImages[i] OR hardImages[i] AND the second panel Image must match the image in easyImages[i] OR hardImages[i]
                    if (((pBoxes[firstPanelNo].Image == easyImages[i]) || (pBoxes[firstPanelNo].Image == hardImages[i])) && ((pBoxes[secondPanelNo].Image == easyImages[i]) || (pBoxes[secondPanelNo].Image == hardImages[i])))
                    {
                        correct = true;
                    }
                }
            }

            Thread.Sleep(500);

            if (correct == true)
            {
                score += Convert.ToInt32(label4.Text) * 4; //Increase the score based on how much time is left. More time == higher score
                panels[firstPanelNo].Visible = false;
                panels[secondPanelNo].Visible = false;
                label2.Text = score.ToString();
                label4.Text = (Convert.ToInt32(label4.Text) + TIMEINCREASE).ToString();

                for (int i = 0; i < panels.Length; i++)
                {
                    if (panels[i].Visible == true)
                    {
                        panels[i].Enabled = true;
                    }
                }
            }
            else
            {
                panels[firstPanelNo].BackgroundImage = panelBackground;
                panels[secondPanelNo].BackgroundImage = panelBackground;
                panels[firstPanelNo].Enabled = true;
                panels[secondPanelNo].Enabled = true;
                pBoxes[firstPanelNo].Visible = false;
                pBoxes[secondPanelNo].Visible = false;


            }
        }

        private void EnableVisiblePanels()
        {
            foreach (Panel panel in panels)
            {
                if (panel.Visible == true)
                {
                    panel.Enabled = true;
                }
            }
        }


        private void timer1_Tick(object sender, EventArgs e) //The Countdown
        {
            int timeLeft = Convert.ToInt32(label4.Text);
            if (timeLeft > 0)
            {
                timeLeft -= 1;
                label4.Text = timeLeft.ToString();
            }
            else
            {
                timer1.Enabled = false;
                DialogResult result = MessageBox.Show("You ran out of time!\r\nWould you like to play again?", "Out Of Time", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    SetUpNewGame();
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }
    }
}
