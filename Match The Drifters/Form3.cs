﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Match_The_Drifters
{
    public partial class Form3 : Form
    {
        string personPath;
        string carPath;
        Bitmap personImage;
        Bitmap carImage;

        public Form3()
        {
            InitializeComponent();
        }

        private void textBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                personPath = openFileDialog1.FileName;
                textBox1.Text = personPath;
            }
        }

        private void textBox2_MouseClick(object sender, MouseEventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                carPath = openFileDialog1.FileName;
                textBox2.Text = carPath;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" || textBox2.Text != "")
            {

                personImage = (Bitmap)Image.FromFile(personPath);
                carImage = (Bitmap)Image.FromFile(carPath);

                int fileName;
                int highScore;

                StreamReader read = new StreamReader(@"Resources\MTD.txt");

                highScore = Convert.ToInt32(read.ReadLine());
                fileName = Convert.ToInt32(read.ReadLine()) + 1;

                read.Close();

                personImage.Save(@"Resources\Images\" + fileName + "-0.bmp");
                carImage.Save(@"Resources\Images\" + fileName + "-1.bmp");

                StreamWriter write = new StreamWriter(@"Resources\MTD.txt");

                write.WriteLine(highScore);
                write.WriteLine(fileName);

                write.Close();

                this.Close();
            }
            else
            {
                MessageBox.Show("You need to choose two images");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
